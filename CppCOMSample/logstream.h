#pragma once

#include <streambuf>
#include <ios>
#include <ostream>
using namespace std;

class logbuf : basic_streambuf<char>
{
	CListBox * lb;
	string buffer;

public:
	void init(CListBox * _lb)
	{
		lb = _lb;
	}

protected:
	virtual int sync()
	{	
		// flush();
		return (0);
	}

	virtual streamsize xsputn(const char *_Ptr, streamsize _Count)
	{
		buffer += _Ptr;
		return _Count;
	}

	virtual int_type overflow(int_type it)
	{	
		if(it == 10)
			flush();
		else
			buffer += it;
		return traits_type::not_eof(it);
	}

	void flush()
	{
		if(lb != NULL)
		{
			lb->AddString(buffer.c_str());
			lb->SetCurSel(lb->GetCount()-1);
			buffer.clear();
		}
	}
};

class logstream
	: public basic_ostream<char>
{
private:
	logbuf m_rdbuf;

public:
	typedef logbuf _Myfb;
	typedef 

	logstream() : std::basic_ostream<char>((basic_streambuf<char>*)&m_rdbuf)
	{}

	~logstream()
	{
	}

	_Myfb *rdbuf() const
	{	// return pointer to file buffer
		return ((_Myfb *)&m_rdbuf);
	}
};

