// TicketNumberDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CppCOMSample.h"
#include "TicketNumberDlg.h"


// CTicketNumberDlg dialog

IMPLEMENT_DYNAMIC(CTicketNumberDlg, CDialog)

CTicketNumberDlg::CTicketNumberDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTicketNumberDlg::IDD, pParent)
	, m_TicketNumber(0)
{

}

CTicketNumberDlg::~CTicketNumberDlg()
{
}

void CTicketNumberDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_QTY, m_TicketNumber);
}


BEGIN_MESSAGE_MAP(CTicketNumberDlg, CDialog)
END_MESSAGE_MAP()


// CTicketNumberDlg message handlers
