// SelectContract.cpp : implementation file
//

#include "stdafx.h"
#include <iostream>
#include "CppCOMSample.h"
#include "SelectContract.h"


// CSelectContract dialog

IMPLEMENT_DYNAMIC(CSelectContract, CDialog)

CSelectContract::CSelectContract(GF_Api_COM::IGFComClientPtr& _GFAPI, GF_Api_COM::ContractKind contractKind, CWnd* pParent /*=NULL*/)
	: CDialog(CSelectContract::IDD, pParent)
	, m_SelectedSymbol(_T(""))
	, m_GFAPI(_GFAPI)
	, m_ContractKind(contractKind)
{

}

CSelectContract::~CSelectContract()
{
}

void CSelectContract::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONTRACTS, m_Symbols);
	DDX_CBString(pDX, IDC_CONTRACTS, m_SelectedSymbol);
}


BEGIN_MESSAGE_MAP(CSelectContract, CDialog)
END_MESSAGE_MAP()


// CSelectContract message handlers
BOOL CSelectContract::OnInitDialog( )
{
	__super::OnInitDialog();
	CWaitCursor cursor;	
	GF_Api_COM::IContractListPtr contracts = m_GFAPI->Contracts->Get();
	int count = 0;
	for(int i=0; i<contracts->Count && count < 100; ++i)
	{
		GF_Api_COM::IContractPtr contract = contracts->GetAt(i);
		if(m_ContractKind == -1 || contract->BaseContract->_ContractKind == m_ContractKind)
		{
			m_Symbols.AddString(contract->Symbol);
			++count;
		}
	}
	if(count > 0)
		m_Symbols.SetCurSel(0);
	return TRUE;
}
