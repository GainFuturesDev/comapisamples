// SymbolLookupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CppCOMSample.h"
#include "SymbolLookupDlg.h"


// CSymbolLookupDlg dialog

IMPLEMENT_DYNAMIC(CSymbolLookupDlg, CDialog)

CSymbolLookupDlg::CSymbolLookupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSymbolLookupDlg::IDD, pParent)
	, m_strLookupText(_T("ES"))
	, m_numContracts(10)
	, m_chkFutures(TRUE)
	, m_chkForex(FALSE)
	, m_indxMode (0)
	, m_Spreads(false)
	, m_chkOptions(false)
{

}

CSymbolLookupDlg::~CSymbolLookupDlg()
{
}

void CSymbolLookupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LOOKUP_TEXT, m_strLookupText);
	DDX_Text(pDX, IDC_NUM_CONTRACTS, m_numContracts);
	DDX_Check(pDX, IDC_CHECK_FUTURES, m_chkFutures);
	DDX_Check(pDX, IDC_CHECK_FX, m_chkForex);
	DDX_Check(pDX, IDC_CHECK_SPREADS, m_Spreads);
	DDX_Check(pDX, IDC_CHECK_OPTIONS, m_chkOptions);
	DDX_CBIndex(pDX, IDC_COMBO_MODE, m_indxMode);
}


BEGIN_MESSAGE_MAP(CSymbolLookupDlg, CDialog)
END_MESSAGE_MAP()


// CSymbolLookupDlg message handlers
