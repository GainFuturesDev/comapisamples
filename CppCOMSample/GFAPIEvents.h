#pragma once
#include <atlbase.h>
#include <atlcom.h>
#include "stdafx.h"

// It is not required in any way to route all events though one object, but this is an example of how to do it.



namespace GF_Api_COM
{
	class CGFAPIEvents;

	extern _ATL_FUNC_INFO OnDisconnectedInfo;
	extern _ATL_FUNC_INFO OnLoginCompleteInfo;
	extern _ATL_FUNC_INFO OnLoginFailedInfo;
	extern _ATL_FUNC_INFO OnAccountSummaryChangedInfo;
	extern _ATL_FUNC_INFO OnBalanceChangedInfo;
	extern _ATL_FUNC_INFO OnAvgPositionChangedInfo;
	extern _ATL_FUNC_INFO OnDetailedPositionChangedInfo;
	extern _ATL_FUNC_INFO OnAllocationBlocksChangedInfo;
	extern _ATL_FUNC_INFO OnLoggedInUserSessionsChangedInfo;
	extern _ATL_FUNC_INFO OnProductCalendarUpdatedInfo;
	extern _ATL_FUNC_INFO OnRuleChangedInfo;
	extern _ATL_FUNC_INFO OnContractsChangedInfo;
	extern _ATL_FUNC_INFO OnContractLoadReceivedInfo;
	extern _ATL_FUNC_INFO OnSymbolLookupReceivedInfo;
	extern _ATL_FUNC_INFO OnContractVolatilityChangedInfo;
	extern _ATL_FUNC_INFO OnVolatilitySubscriptionChangedInfo;
	extern _ATL_FUNC_INFO OnCurrencyPriceChangedInfo;
	extern _ATL_FUNC_INFO OnErrorOccurredInfo;
	extern _ATL_FUNC_INFO OnNewMessageLoggedInfo;
	extern _ATL_FUNC_INFO OnMarginCalculationCompletedInfo;
	extern _ATL_FUNC_INFO OnPortfolioMarginChangedInfo;
	extern _ATL_FUNC_INFO OnChatMessageReceivedInfo;
	extern _ATL_FUNC_INFO OnNotificationMessageReceivedInfo;
	extern _ATL_FUNC_INFO OnCommandUpdatedInfo;
	extern _ATL_FUNC_INFO OnOrderConfirmedInfo;
	extern _ATL_FUNC_INFO OnOrderFilledInfo;
	extern _ATL_FUNC_INFO OnOrderStateChangedInfo;
	extern _ATL_FUNC_INFO OnStrategyChangedInfo;
	extern _ATL_FUNC_INFO OnStrategyConfirmedInfo;
	extern _ATL_FUNC_INFO OnStrategyStartedInfo;
	extern _ATL_FUNC_INFO OnStrategyStoppedInfo;
	extern _ATL_FUNC_INFO OnBarsReceivedInfo;
	extern _ATL_FUNC_INFO OnDayBarsReceivedInfo;
	extern _ATL_FUNC_INFO OnDomChangedInfo;
	extern _ATL_FUNC_INFO OnHistogramReceivedInfo;
	extern _ATL_FUNC_INFO OnPriceChangedInfo;
	extern _ATL_FUNC_INFO OnPriceTickInfo;
	extern _ATL_FUNC_INFO OnTicksReceivedInfo;
	extern _ATL_FUNC_INFO OnTraderErrorInfo;
	extern _ATL_FUNC_INFO OnTradersChangedInfo;
	extern _ATL_FUNC_INFO OnRelationsChangedInfo;
	extern _ATL_FUNC_INFO OnUserStatusChangedInfo;

	typedef IDispEventSimpleImpl<0, CGFAPIEvents, &__uuidof(IAccountsApiEvents)> IDispEventSimpleImpl_IAccountsApiEvents;
	typedef IDispEventSimpleImpl<1, CGFAPIEvents, &__uuidof(IAllocationApiEvents)> IDispEventSimpleImpl_IAllocationApiEvents;
	typedef IDispEventSimpleImpl<2, CGFAPIEvents, &__uuidof(IServerConnectionApiEvents)> IDispEventSimpleImpl_IServerConnectionApiEvents;
	typedef IDispEventSimpleImpl<3, CGFAPIEvents, &__uuidof(ISessionsApiEvents)> IDispEventSimpleImpl_ISessionsApiEvents;
	typedef IDispEventSimpleImpl<4, CGFAPIEvents, &__uuidof(IContinuousContractsApiEvents)> IDispEventSimpleImpl_IContinuousContractsApiEvents;
	typedef IDispEventSimpleImpl<5, CGFAPIEvents, &__uuidof(IContractsApiEvents)> IDispEventSimpleImpl_IContractsApiEvents;
	typedef IDispEventSimpleImpl<6, CGFAPIEvents, &__uuidof(IContractLoadApiEvents)> IDispEventSimpleImpl_IContractLoadApiEvents;
	typedef IDispEventSimpleImpl<7, CGFAPIEvents, &__uuidof(ISymbolLookupApiEvents)> IDispEventSimpleImpl_ISymbolLookupApiEvents;
	typedef IDispEventSimpleImpl<8, CGFAPIEvents, &__uuidof(IContractVolatilityApiEvents)> IDispEventSimpleImpl_IContractVolatilityApiEvents;
	typedef IDispEventSimpleImpl<9, CGFAPIEvents, &__uuidof(ICurrencyApiEvents)> IDispEventSimpleImpl_ICurrencyApiEvents;
	typedef IDispEventSimpleImpl<10, CGFAPIEvents, &__uuidof(ILoggingApiEvents)> IDispEventSimpleImpl_ILoggingApiEvents;
	typedef IDispEventSimpleImpl<11, CGFAPIEvents, &__uuidof(IMarginApiEvents)> IDispEventSimpleImpl_IMarginApiEvents;
	typedef IDispEventSimpleImpl<12, CGFAPIEvents, &__uuidof(IChatApiEvent)> IDispEventSimpleImpl_IChatApiEvent;
	typedef IDispEventSimpleImpl<13, CGFAPIEvents, &__uuidof(INotificationsApiEvents)> IDispEventSimpleImpl_INotificationsApiEvents;
	typedef IDispEventSimpleImpl<14, CGFAPIEvents, &__uuidof(IOrdersApiEvents)> IDispEventSimpleImpl_IOrdersApiEvents;
	typedef IDispEventSimpleImpl<15, CGFAPIEvents, &__uuidof(IStrategiesApiEvents)> IDispEventSimpleImpl_IStrategiesApiEvents;
	typedef IDispEventSimpleImpl<16, CGFAPIEvents, &__uuidof(IBarSubscriptionApiEvents)> IDispEventSimpleImpl_IBarSubscriptionApiEvents;
	typedef IDispEventSimpleImpl<17, CGFAPIEvents, &__uuidof(IDomSubscriptionApiEvents)> IDispEventSimpleImpl_IDomSubscriptionApiEvents;
	typedef IDispEventSimpleImpl<18, CGFAPIEvents, &__uuidof(IHistogramSubscriptionApiEvents)> IDispEventSimpleImpl_IHistogramSubscriptionApiEvents;
	typedef IDispEventSimpleImpl<19, CGFAPIEvents, &__uuidof(IPriceSubscriptionApiEvents)> IDispEventSimpleImpl_IPriceSubscriptionApiEvents;
	typedef IDispEventSimpleImpl<20, CGFAPIEvents, &__uuidof(ITickSubscriptionApiEvents)> IDispEventSimpleImpl_ITickSubscriptionApiEvents;
	typedef IDispEventSimpleImpl<21, CGFAPIEvents, &__uuidof(ITradersApiEvents)> IDispEventSimpleImpl_ITradersApiEvents;
	typedef IDispEventSimpleImpl<22, CGFAPIEvents, &__uuidof(IUsersApiEvents)> IDispEventSimpleImpl_IUsersApiEvents;

	class CGFAPIEvents :
		IDispEventSimpleImpl_IAccountsApiEvents
		,IDispEventSimpleImpl_IAllocationApiEvents
		,IDispEventSimpleImpl_IServerConnectionApiEvents
		,IDispEventSimpleImpl_ISessionsApiEvents
		,IDispEventSimpleImpl_IContinuousContractsApiEvents
		,IDispEventSimpleImpl_IContractsApiEvents
		,IDispEventSimpleImpl_IContractLoadApiEvents
		,IDispEventSimpleImpl_ISymbolLookupApiEvents
		,IDispEventSimpleImpl_IContractVolatilityApiEvents
		,IDispEventSimpleImpl_ICurrencyApiEvents
		,IDispEventSimpleImpl_ILoggingApiEvents
		,IDispEventSimpleImpl_IMarginApiEvents
		,IDispEventSimpleImpl_IChatApiEvent
		,IDispEventSimpleImpl_INotificationsApiEvents
		,IDispEventSimpleImpl_IOrdersApiEvents
		,IDispEventSimpleImpl_IStrategiesApiEvents
		,IDispEventSimpleImpl_IBarSubscriptionApiEvents
		,IDispEventSimpleImpl_IDomSubscriptionApiEvents
		,IDispEventSimpleImpl_IHistogramSubscriptionApiEvents
		,IDispEventSimpleImpl_IPriceSubscriptionApiEvents
		,IDispEventSimpleImpl_ITickSubscriptionApiEvents
		,IDispEventSimpleImpl_ITradersApiEvents
		,IDispEventSimpleImpl_IUsersApiEvents
	{
	protected:
		IGFComClientPtr server;

	public:
		BEGIN_SINK_MAP(CGFAPIEvents)
			SINK_ENTRY_INFO(0, __uuidof(IAccountsApiEvents), 1, OnAccountSummaryChanged_, &OnAccountSummaryChangedInfo)
			SINK_ENTRY_INFO(0, __uuidof(IAccountsApiEvents), 2, OnBalanceChanged_, &OnBalanceChangedInfo)
			SINK_ENTRY_INFO(0, __uuidof(IAccountsApiEvents), 3, OnAvgPositionChanged_, &OnAvgPositionChangedInfo)
			SINK_ENTRY_INFO(0, __uuidof(IAccountsApiEvents), 4, OnDetailedPositionChanged_, &OnDetailedPositionChangedInfo)

			SINK_ENTRY_INFO(1, __uuidof(IAllocationApiEvents), 1, OnAllocationBlocksChanged_, &OnAllocationBlocksChangedInfo)

			SINK_ENTRY_INFO(2, __uuidof(IServerConnectionApiEvents), 1, OnDisconnected_, &OnDisconnectedInfo)
			SINK_ENTRY_INFO(2, __uuidof(IServerConnectionApiEvents), 2, OnLoginComplete_, &OnLoginCompleteInfo)
			SINK_ENTRY_INFO(2, __uuidof(IServerConnectionApiEvents), 3, OnLoginFailed_, &OnLoginFailedInfo)

			SINK_ENTRY_INFO(3, __uuidof(ISessionsApiEvents), 1, OnLoggedInUserSessionsChanged_, &OnLoggedInUserSessionsChangedInfo)

			SINK_ENTRY_INFO(4, __uuidof(IContinuousContractsApiEvents), 1, OnProductCalendarUpdated_, &OnProductCalendarUpdatedInfo)
			SINK_ENTRY_INFO(4, __uuidof(IContinuousContractsApiEvents), 2, OnRuleChanged_, &OnRuleChangedInfo)

			SINK_ENTRY_INFO(5, __uuidof(IContractsApiEvents), 1, OnContractsChanged_, &OnContractsChangedInfo)

			SINK_ENTRY_INFO(6, __uuidof(IContractLoadApiEvents), 1, OnContractLoadReceived_, &OnContractLoadReceivedInfo)

			SINK_ENTRY_INFO(7, __uuidof(ISymbolLookupApiEvents), 1, OnSymbolLookupReceived_, &OnSymbolLookupReceivedInfo)

			SINK_ENTRY_INFO(8, __uuidof(IContractVolatilityApiEvents), 1, OnContractVolatilityChanged_, &OnContractVolatilityChangedInfo)

			SINK_ENTRY_INFO(9, __uuidof(ICurrencyApiEvents), 1, OnCurrencyPriceChanged_, &OnCurrencyPriceChangedInfo)

			SINK_ENTRY_INFO(10, __uuidof(ILoggingApiEvents), 1, OnErrorOccurred_, &OnErrorOccurredInfo)
			SINK_ENTRY_INFO(10, __uuidof(ILoggingApiEvents), 2, OnNewMessageLogged_, &OnNewMessageLoggedInfo)

			SINK_ENTRY_INFO(11, __uuidof(IMarginApiEvents), 1, OnMarginCalculationCompleted_, &OnMarginCalculationCompletedInfo)
			SINK_ENTRY_INFO(11, __uuidof(IMarginApiEvents), 2, OnPortfolioMarginChanged_, &OnPortfolioMarginChangedInfo)

			SINK_ENTRY_INFO(12, __uuidof(IChatApiEvent), 1, OnChatMessageReceived_, &OnChatMessageReceivedInfo)

			SINK_ENTRY_INFO(13, __uuidof(INotificationsApiEvents), 1, OnNotificationMessageReceived_, &OnNotificationMessageReceivedInfo)

			SINK_ENTRY_INFO(14, __uuidof(IOrdersApiEvents), 1, OnCommandUpdated_, &OnCommandUpdatedInfo)
			SINK_ENTRY_INFO(14, __uuidof(IOrdersApiEvents), 2, OnOrderConfirmed_, &OnOrderConfirmedInfo)
			SINK_ENTRY_INFO(14, __uuidof(IOrdersApiEvents), 3, OnOrderFilled_, &OnOrderFilledInfo)
			SINK_ENTRY_INFO(14, __uuidof(IOrdersApiEvents), 4, OnOrderStateChanged_, &OnOrderStateChangedInfo)

			SINK_ENTRY_INFO(15, __uuidof(IStrategiesApiEvents), 1, OnStrategyChanged_, &OnStrategyChangedInfo)
			SINK_ENTRY_INFO(15, __uuidof(IStrategiesApiEvents), 2, OnStrategyConfirmed_, &OnStrategyConfirmedInfo)
			SINK_ENTRY_INFO(15, __uuidof(IStrategiesApiEvents), 3, OnStrategyStarted_, &OnStrategyStartedInfo)
			SINK_ENTRY_INFO(15, __uuidof(IStrategiesApiEvents), 4, OnStrategyStopped_, &OnStrategyStoppedInfo)

			SINK_ENTRY_INFO(16, __uuidof(IBarSubscriptionApiEvents), 1, OnBarsReceived_, &OnBarsReceivedInfo)
			SINK_ENTRY_INFO(16, __uuidof(IBarSubscriptionApiEvents), 2, OnDayBarsReceived_, &OnDayBarsReceivedInfo)

			SINK_ENTRY_INFO(17, __uuidof(IDomSubscriptionApiEvents), 1, OnDomChanged_, &OnDomChangedInfo)

			SINK_ENTRY_INFO(18, __uuidof(IHistogramSubscriptionApiEvents), 1, OnHistogramReceived_, &OnHistogramReceivedInfo)

			SINK_ENTRY_INFO(19, __uuidof(IPriceSubscriptionApiEvents), 1, OnPriceChanged_, &OnPriceChangedInfo)
			SINK_ENTRY_INFO(19, __uuidof(IPriceSubscriptionApiEvents), 2, OnPriceTick_, &OnPriceTickInfo)

			SINK_ENTRY_INFO(20, __uuidof(ITickSubscriptionApiEvents), 1, OnTicksReceived_, &OnTicksReceivedInfo)

			SINK_ENTRY_INFO(21, __uuidof(ITradersApiEvents), 1, OnTraderError_, &OnTraderErrorInfo)
			SINK_ENTRY_INFO(21, __uuidof(ITradersApiEvents), 2, OnTradersChanged_, &OnTradersChangedInfo)

			SINK_ENTRY_INFO(22, __uuidof(IUsersApiEvents), 1, OnRelationsChanged_, &OnRelationsChangedInfo)
			SINK_ENTRY_INFO(22, __uuidof(IUsersApiEvents), 2, OnUserStatusChanged_, &OnUserStatusChangedInfo)
		END_SINK_MAP()

		//IAccountsApiEvents
		virtual void __stdcall OnAccountSummaryChanged(IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency) {}
		virtual void __stdcall OnBalanceChanged(IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency) {}
		virtual void __stdcall OnAvgPositionChanged(IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::IPositionPtr contractPosition) {}
		virtual void __stdcall OnDetailedPositionChanged(IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::IPositionPtr contractPosition) {}

		//IAllocationApiEvents
		virtual void __stdcall OnAllocationBlocksChanged(IGFComClientPtr client, GF_Api_COM::IAllocationBlockTemplateListPtr allocationBlockTemplates) {}

		//IServerConnectionApiEvents
		virtual void __stdcall OnDisconnected(DisconnectionReason reason, BSTR message) {}
		virtual void __stdcall OnLoginComplete() {}
		virtual void __stdcall OnLoginFailed(FailReason failReason) {}

		//ISessionsApiEvents
		virtual void __stdcall OnLoggedInUserSessionsChanged(IGFComClientPtr client, GF_Api_COM::ILoggedInUserSessionListPtr loggedInUserSessions) {}

		//IContinuousContractsApiEvents
		virtual void __stdcall OnProductCalendarUpdated(IGFComClientPtr client, GF_Api_COM::IBaseContractPtr baseContract) {}
		virtual void __stdcall OnRuleChanged(IGFComClientPtr client, GF_Api_COM::IContinuousContractRulePtr rule) {}

		//IContractsApiEvents
		virtual void __stdcall OnContractsChanged(IGFComClientPtr client, GF_Api_COM::IBaseContractPtr baseContract) {}

		//IContractLoadApiEvents
		virtual void __stdcall OnContractLoadReceived(IGFComClientPtr client, GF_Api_COM::IContractLoadRequestIDPtr requestID, GF_Api_COM::IContractListPtr contracts) {}

		//ISymbolLookupApiEvents
		virtual void __stdcall OnSymbolLookupReceived(IGFComClientPtr client, GF_Api_COM::ISymbolLookupRequestIDPtr requestID, GF_Api_COM::IContractListPtr contracts) {}

		//IContractVolatilityApiEvents
		virtual void __stdcall OnContractVolatilityChanged(IGFComClientPtr client, GF_Api_COM::IContractVolatilityPtr volatility) {}
		virtual void __stdcall OnVolatilitySubscriptionChanged(IGFComClientPtr client, bool isSubscribed, GF_Api_COM::IContractVolatilityListPtr volatilities) {}

		//ICurrencyApiEvents
		virtual void __stdcall OnCurrencyPriceChanged(IGFComClientPtr client, GF_Api_COM::ICurrencyPtr currency, GF_Api_COM::IPricePtr price) {}

		//ILoggingApiEvents
		virtual void __stdcall OnErrorOccurred(IGFComClientPtr client, mscorlib::_ExceptionPtr exception) {}
		virtual void __stdcall OnNewMessageLogged(IGFComClientPtr client, LogCategory category, BSTR message) {}

		//IMarginApiEvents
		virtual void __stdcall OnMarginCalculationCompleted(IGFComClientPtr client, GF_Api_COM::IMarginCalculationResultPtr result) {}
		virtual void __stdcall OnPortfolioMarginChanged(IGFComClientPtr client, GF_Api_COM::IAccountPtr account) {}

		//IChatApiEvent
		virtual void __stdcall OnChatMessageReceived(IGFComClientPtr client, GF_Api_COM::IChatMessagePtr chatMessage) {}

		//INotificationsApiEvents
		virtual void __stdcall OnNotificationMessageReceived(IGFComClientPtr client, GF_Api_COM::INotificationMessagePtr notificationMessage) {}

		//IOrdersApiEvents
		virtual void __stdcall OnCommandUpdated(IGFComClientPtr client, IOrderPtr order, GF_Api_COM::ICommandPtr command) {}
		virtual void __stdcall OnOrderConfirmed(IGFComClientPtr client, IOrderPtr order, GF_Api_COM::IOrderIDPtr originalOrderId) {}
		virtual void __stdcall OnOrderFilled(IGFComClientPtr client, IOrderPtr order, GF_Api_COM::IFillPtr fill) {}
		virtual void __stdcall OnOrderStateChanged(IGFComClientPtr client, IOrderPtr order, OrderState previousState) {}

		//IStrategiesApiEvents
		virtual void __stdcall OnStrategyChanged(IGFComClientPtr client, GF_Api_COM::IStrategyPtr strategy) {}
		virtual void __stdcall OnStrategyConfirmed(IGFComClientPtr client, GF_Api_COM::IStrategyPtr strategy, int localID) {}
		virtual void __stdcall OnStrategyStarted(IGFComClientPtr client, GF_Api_COM::IStrategyPtr strategy) {}
		virtual void __stdcall OnStrategyStopped(IGFComClientPtr client, GF_Api_COM::IStrategyPtr strategy, ServerStrategyStopReason reason) {}

		//IBarSubscriptionApiEvents
		virtual void __stdcall OnBarsReceived(IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::IBarListPtr bars) {}
		virtual void __stdcall OnDayBarsReceived(IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::IBarListPtr bars) {}

		//IDomSubscriptionApiEvents
		virtual void __stdcall OnDomChanged(IGFComClientPtr client, IContractPtr contract) {}

		//IHistogramSubscriptionApiEvents
		virtual void __stdcall OnHistogramReceived(IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::IHistogramPtr histogram) {}

		//IPriceSubscriptionApiEvents
		virtual void __stdcall OnPriceChanged(IGFComClientPtr client, GF_Api_COM::IContractPtr contract, GF_Api_COM::IPricePtr price) {}
		virtual void __stdcall OnPriceTick(IGFComClientPtr client, GF_Api_COM::IContractPtr contract, GF_Api_COM::IPricePtr price) {}

		//ITickSubscriptionApiEvents
		virtual void __stdcall OnTicksReceived(IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::ITickListPtr ticks) {}

		//ITradersApiEvents
		virtual void __stdcall OnTraderError(IGFComClientPtr client, GF_Api_COM::ITraderPtr trader, BSTR message) {}
		virtual void __stdcall OnTradersChanged(IGFComClientPtr client) {}

		//IUsersApiEvents
		virtual void __stdcall OnRelationsChanged(IGFComClientPtr client) {}
		virtual void __stdcall OnUserStatusChanged(IGFComClientPtr client, GF_Api_COM::IUserPtr user) {}


		BOOL AttachClient(IGFComClientPtr client)
		{
			if (client == NULL || server != NULL)
				return FALSE;

			IDispEventSimpleImpl_IAccountsApiEvents::DispEventAdvise(client->Accounts);
			IDispEventSimpleImpl_IAllocationApiEvents::DispEventAdvise(client->Allocation);
			IDispEventSimpleImpl_IServerConnectionApiEvents::DispEventAdvise(client->Connection->Aggregate);
			IDispEventSimpleImpl_ISessionsApiEvents::DispEventAdvise(client->Connection->Sessions);
			IDispEventSimpleImpl_IContinuousContractsApiEvents::DispEventAdvise(client->Contracts->Continuous);
			IDispEventSimpleImpl_IContractsApiEvents::DispEventAdvise(client->Contracts);
			IDispEventSimpleImpl_IContractLoadApiEvents::DispEventAdvise(client->Contracts->Load);
			IDispEventSimpleImpl_ISymbolLookupApiEvents::DispEventAdvise(client->Contracts->Lookup);
			IDispEventSimpleImpl_IContractVolatilityApiEvents::DispEventAdvise(client->Contracts->Volatility);
			IDispEventSimpleImpl_ICurrencyApiEvents::DispEventAdvise(client->Currencies);
			IDispEventSimpleImpl_ILoggingApiEvents::DispEventAdvise(client->Logging);
			IDispEventSimpleImpl_IMarginApiEvents::DispEventAdvise(client->Margin);
			IDispEventSimpleImpl_IChatApiEvent::DispEventAdvise(client->Messaging->Chat);
			IDispEventSimpleImpl_INotificationsApiEvents::DispEventAdvise(client->Messaging->Notifications);
			IDispEventSimpleImpl_IOrdersApiEvents::DispEventAdvise(client->Orders);
			IDispEventSimpleImpl_IStrategiesApiEvents::DispEventAdvise(client->Strategies);
			IDispEventSimpleImpl_IBarSubscriptionApiEvents::DispEventAdvise(client->Subscriptions->Bars);
			IDispEventSimpleImpl_IDomSubscriptionApiEvents::DispEventAdvise(client->Subscriptions->Dom);
			IDispEventSimpleImpl_IHistogramSubscriptionApiEvents::DispEventAdvise(client->Subscriptions->Histogram);
			IDispEventSimpleImpl_IPriceSubscriptionApiEvents::DispEventAdvise(client->Subscriptions->Price);
			IDispEventSimpleImpl_ITickSubscriptionApiEvents::DispEventAdvise(client->Subscriptions->Ticks);
			IDispEventSimpleImpl_ITradersApiEvents::DispEventAdvise(client->Traders);
			IDispEventSimpleImpl_IUsersApiEvents::DispEventAdvise(client->Users);

			server = client;
			return TRUE;
		}


		BOOL DetachClient()
		{
			if (server == NULL)
				return FALSE;

			IDispEventSimpleImpl_IAccountsApiEvents::DispEventUnadvise(server->Accounts);
			IDispEventSimpleImpl_IAllocationApiEvents::DispEventUnadvise(server->Allocation);
			IDispEventSimpleImpl_IServerConnectionApiEvents::DispEventUnadvise(server->Connection->Aggregate);
			IDispEventSimpleImpl_ISessionsApiEvents::DispEventUnadvise(server->Connection->Sessions);
			IDispEventSimpleImpl_IContinuousContractsApiEvents::DispEventUnadvise(server->Contracts->Continuous);
			IDispEventSimpleImpl_IContractsApiEvents::DispEventUnadvise(server->Contracts);
			IDispEventSimpleImpl_IContractLoadApiEvents::DispEventUnadvise(server->Contracts->Load);
			IDispEventSimpleImpl_ISymbolLookupApiEvents::DispEventUnadvise(server->Contracts->Lookup);
			IDispEventSimpleImpl_IContractVolatilityApiEvents::DispEventUnadvise(server->Contracts->Volatility);
			IDispEventSimpleImpl_ICurrencyApiEvents::DispEventUnadvise(server->Currencies);
			IDispEventSimpleImpl_ILoggingApiEvents::DispEventUnadvise(server->Logging);
			IDispEventSimpleImpl_IMarginApiEvents::DispEventUnadvise(server->Margin);
			IDispEventSimpleImpl_IChatApiEvent::DispEventUnadvise(server->Messaging->Chat);
			IDispEventSimpleImpl_INotificationsApiEvents::DispEventUnadvise(server->Messaging->Notifications);
			IDispEventSimpleImpl_IOrdersApiEvents::DispEventUnadvise(server->Orders);
			IDispEventSimpleImpl_IStrategiesApiEvents::DispEventUnadvise(server->Strategies);
			IDispEventSimpleImpl_IBarSubscriptionApiEvents::DispEventUnadvise(server->Subscriptions->Bars);
			IDispEventSimpleImpl_IDomSubscriptionApiEvents::DispEventUnadvise(server->Subscriptions->Dom);
			IDispEventSimpleImpl_IHistogramSubscriptionApiEvents::DispEventUnadvise(server->Subscriptions->Histogram);
			IDispEventSimpleImpl_IPriceSubscriptionApiEvents::DispEventUnadvise(server->Subscriptions->Price);
			IDispEventSimpleImpl_ITickSubscriptionApiEvents::DispEventUnadvise(server->Subscriptions->Ticks);
			IDispEventSimpleImpl_ITradersApiEvents::DispEventUnadvise(server->Traders);
			IDispEventSimpleImpl_IUsersApiEvents::DispEventUnadvise(server->Users);

			server = NULL;
			return TRUE;
		}

	private:

		//IAccountsApiEvents
		virtual void __stdcall OnAccountSummaryChanged_(LPDISPATCH client, LPDISPATCH account, LPDISPATCH currency)
		{
			OnAccountSummaryChanged(client, account, currency);
		}

		virtual void __stdcall OnBalanceChanged_(LPDISPATCH client, LPDISPATCH account, LPDISPATCH currency)
		{
			OnBalanceChanged(client, account, currency);
		}

		void __stdcall OnAvgPositionChanged_(LPDISPATCH client, LPDISPATCH account, LPDISPATCH contractPosition)
		{
			OnAvgPositionChanged(client, account, contractPosition);
		}

		virtual void __stdcall OnDetailedPositionChanged_(LPDISPATCH client, LPDISPATCH account, LPDISPATCH contractPosition)
		{
			OnDetailedPositionChanged(client, account, contractPosition);
		}


		//IAllocationApiEvents
		virtual void __stdcall OnAllocationBlocksChanged_(LPDISPATCH client, LPDISPATCH allocationBlockTemplates)
		{
			OnAllocationBlocksChanged(client, allocationBlockTemplates);
		}


		//IServerConnectionApiEvents
		virtual void __stdcall OnDisconnected_(DisconnectionReason reason, BSTR message)
		{
			OnDisconnected(reason, message);
		}

		virtual void __stdcall OnLoginComplete_()
		{
			OnLoginComplete();
		}

		virtual void __stdcall OnLoginFailed_(FailReason failReason)
		{
			OnLoginFailed(failReason);
		}


		//ISessionsApiEvents
		virtual void __stdcall OnLoggedInUserSessionsChanged_(LPDISPATCH client, LPDISPATCH loggedInUserSessions)
		{
			OnLoggedInUserSessionsChanged(client, loggedInUserSessions);
		}


		//IContinuousContractsApiEvents
		virtual void __stdcall OnProductCalendarUpdated_(LPDISPATCH client, LPDISPATCH baseContract)
		{
			OnProductCalendarUpdated(client, baseContract);
		}

		virtual void __stdcall OnRuleChanged_(LPDISPATCH client, LPDISPATCH rule)
		{
			OnRuleChanged(client, rule);
		}


		//IContractsApiEvents
		virtual void __stdcall OnContractsChanged_(LPDISPATCH client, LPDISPATCH baseContract)
		{
			OnContractsChanged(client, baseContract);
		}


		//IContractLoadApiEvents
		virtual void __stdcall OnContractLoadReceived_(LPDISPATCH client, LPDISPATCH requestID, LPDISPATCH contracts)
		{
			OnContractLoadReceived(client, requestID, contracts);
		}


		//ISymbolLookupApiEvents
		virtual void __stdcall OnSymbolLookupReceived_(LPDISPATCH client, LPDISPATCH requestID, LPDISPATCH contracts)
		{
			OnSymbolLookupReceived(client, requestID, contracts);
		}


		//IContractVolatilityApiEvents
		virtual void __stdcall OnContractVolatilityChanged_(LPDISPATCH client, LPDISPATCH volatility)
		{
			OnContractVolatilityChanged(client, volatility);
		}

		virtual void __stdcall OnVolatilitySubscriptionChanged_(LPDISPATCH client, bool isSubscribed, LPDISPATCH volatilities)
		{
			OnVolatilitySubscriptionChanged(client, isSubscribed, volatilities);
		}


		//ICurrencyApiEvents
		virtual void __stdcall OnCurrencyPriceChanged_(LPDISPATCH client, LPDISPATCH currency, LPDISPATCH price)
		{
			OnCurrencyPriceChanged(client, currency, price);
		}


		//ILoggingApiEvents
		virtual void __stdcall OnErrorOccurred_(LPDISPATCH client, LPDISPATCH exception)
		{
			OnErrorOccurred(client, exception);
		}

		virtual void __stdcall OnNewMessageLogged_(LPDISPATCH client, LogCategory category, BSTR message)
		{
			OnNewMessageLogged(client, category, message);
		}


		//IMarginApiEvents
		virtual void __stdcall OnMarginCalculationCompleted_(LPDISPATCH client, LPDISPATCH result)
		{
			OnMarginCalculationCompleted(client, result);
		}

		virtual void __stdcall OnPortfolioMarginChanged_(LPDISPATCH client, LPDISPATCH account)
		{
			OnPortfolioMarginChanged(client, account);
		}


		//IChatApiEvent
		virtual void __stdcall OnChatMessageReceived_(LPDISPATCH client, LPDISPATCH chatMessage)
		{
			OnChatMessageReceived(client, chatMessage);
		}


		//INotificationsApiEvents
		virtual void __stdcall OnNotificationMessageReceived_(LPDISPATCH client, LPDISPATCH notificationMessage)
		{
			OnNotificationMessageReceived(client, notificationMessage);
		}


		//IOrdersApiEvents
		virtual void __stdcall OnCommandUpdated_(LPDISPATCH client, LPDISPATCH order, LPDISPATCH command)
		{
			OnCommandUpdated(client, order, command);
		}

		virtual void __stdcall OnOrderConfirmed_(LPDISPATCH client, LPDISPATCH order, LPDISPATCH originalOrderId)
		{
			OnOrderConfirmed(client, order, originalOrderId);
		}

		virtual void __stdcall OnOrderFilled_(LPDISPATCH client, LPDISPATCH order, LPDISPATCH fill)
		{
			OnOrderFilled(client, order, fill);
		}

		virtual void __stdcall OnOrderStateChanged_(LPDISPATCH client, LPDISPATCH order, OrderState previousState)
		{
			OnOrderStateChanged(client, order, previousState);
		}


		//IStrategiesApiEvents
		virtual void __stdcall OnStrategyChanged_(LPDISPATCH client, LPDISPATCH strategy)
		{
			OnStrategyChanged(client, strategy);
		}

		virtual void __stdcall OnStrategyConfirmed_(LPDISPATCH client, LPDISPATCH strategy, int localID)
		{
			OnStrategyConfirmed(client, strategy, localID);
		}

		virtual void __stdcall OnStrategyStarted_(LPDISPATCH client, LPDISPATCH strategy)
		{
			OnStrategyStarted(client, strategy);
		}

		virtual void __stdcall OnStrategyStopped_(LPDISPATCH client, LPDISPATCH strategy, ServerStrategyStopReason reason)
		{
			OnStrategyStopped(client, strategy, reason);
		}


		//IBarSubscriptionApiEvents
		virtual void __stdcall OnBarsReceived_(LPDISPATCH client, LPDISPATCH subscription, LPDISPATCH bars)
		{
			OnBarsReceived(client, subscription, bars);
		}

		virtual void __stdcall OnDayBarsReceived_(LPDISPATCH client, LPDISPATCH subscription, LPDISPATCH bars)
		{
			OnDayBarsReceived(client, subscription, bars);
		}


		//IDomSubscriptionApiEvents
		virtual void __stdcall OnDomChanged_(LPDISPATCH client, LPDISPATCH contract)
		{
			OnDomChanged(client, contract);
		}


		//IHistogramSubscriptionApiEvents
		virtual void __stdcall OnHistogramReceived_(LPDISPATCH client, LPDISPATCH subscription, LPDISPATCH histogram)
		{
			OnHistogramReceived(client, subscription, histogram);
		}


		//IPriceSubscriptionApiEvents
		virtual void __stdcall OnPriceChanged_(LPDISPATCH client, LPDISPATCH contract, LPDISPATCH price)
		{
			OnPriceChanged(client, contract, price);
		}

		virtual void __stdcall OnPriceTick_(LPDISPATCH client, LPDISPATCH contract, LPDISPATCH price)
		{
			OnPriceTick(client, contract, price);
		}


		//ITickSubscriptionApiEvents
		virtual void __stdcall OnTicksReceived_(LPDISPATCH client, LPDISPATCH subscription, LPDISPATCH ticks)
		{
			OnTicksReceived(client, subscription, ticks);
		}


		//ITradersApiEvents
		virtual void __stdcall OnTraderError_(LPDISPATCH client, LPDISPATCH trader, BSTR message)
		{
			OnTraderError(client, trader, message);
		}

		virtual void __stdcall OnTradersChanged_(LPDISPATCH client)
		{
			OnTradersChanged(client);
		}


		//IUsersApiEvents
		virtual void __stdcall OnRelationsChanged_(LPDISPATCH client)
		{
			OnRelationsChanged(client);
		}

		virtual void __stdcall OnUserStatusChanged_(LPDISPATCH client, LPDISPATCH user)
		{
			OnUserStatusChanged(client, user);
		}
	};
}