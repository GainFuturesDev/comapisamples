//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CppCOMSample.rc
//
#define IDD_CPPCOMSAMPLE_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_LOGIN                       129
#define IDD_ORDER                       130
#define IDD_SELECTCONTRACT              131
#define IDD_ORDERID                     132
#define IDD_LOOKUP                      133
#define IDC_LOG                         1000
#define IDC_CONNECT                     1001
#define IDC_SENDORDER                   1002
#define IDC_SUBSCRIBEPRICE              1003
#define IDC_USERNAME                    1004
#define IDC_SUBSCRIBEDOM                1004
#define IDC_PASSWORD                    1005
#define IDC_HISTORY                     1005
#define IDC_BUY                         1006
#define IDC_CONTRACTDETAILS             1006
#define IDC_USERNAME2                   1006
#define IDC_SERVER                      1006
#define IDC_SELL                        1007
#define IDC_DISCONNECT                  1007
#define IDC_QTY                         1008
#define IDC_CONTRACTS                   1009
#define IDC_TYPE                        1010
#define IDC_PRICE                       1011
#define IDC_GTC                         1012
#define IDC_SHOWPRICES                  1014
#define IDC_SHOWPOSITIONS               1015
#define IDC_SHOWBALANCES                1016
#define IDC_SHOWDOM                     1017
#define IDC_SHOWORDERS                  1018
#define IDC_CANCELORDER                 1019
#define IDC_MODIFYORDER                 1020
#define IDC_SHOWPOSI                    1021
#define IDC_SHOWPOS                     1021
#define IDC_BTNLOOKUP                   1022
#define IDC_EDIT_LOOKUP_TEXT            1023
#define IDC_BTNQUOTEDETAILS             1023
#define IDC_BTNINDEX                    1024
#define IDC_NUM_CONTRACTS               1025
#define IDC_CHECK_FUTURES               1026
#define IDC_CHECK_FX                    1027
#define IDC_CHECK_EQUITY                1028
#define IDC_CHECK_SPREADS               1028
#define IDC_CHECK_EQUITY2               1029
#define IDC_CHECK_EQUITY_INDEX          1029
#define IDC_CHECK_FX2                   1029
#define IDC_CHECK_OPTIONS               1029
#define IDC_COMBO_MODE                  1033
#define IDC_CHECK_AB                    1034
#define IDC_BTNACCOUNTINFO              1035
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
