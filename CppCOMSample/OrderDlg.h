#pragma once
#include "afxwin.h"


// COrderDlg dialog

class COrderDlg : public CDialog
{
	DECLARE_DYNAMIC(COrderDlg)

public:
	COrderDlg(GF_Api_COM::IGFComClientPtr& _GFAPI, GF_Api_COM::IOrderPtr _Original = NULL, CWnd* pParent = NULL);   // standard constructor
	virtual ~COrderDlg();

// Dialog Data
	enum { IDD = IDD_ORDER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual  BOOL OnInitDialog( );

	DECLARE_MESSAGE_MAP()
public:
	GF_Api_COM::IGFComClientPtr m_GFAPI;
	GF_Api_COM::IOrderPtr m_Original;
	void Initialize();
	int m_Qty;
	CString m_SelectedContract;
	int m_SelectedType;
	double m_Price;
	BOOL m_GTC;
	BOOL m_Buy;
	afx_msg void OnBnClickedOk();
	CComboBox m_Symbols;
	CComboBox m_Types;
	afx_msg void OnCbnSelchangeContracts();
	BOOL m_AllocationBlock;
};
