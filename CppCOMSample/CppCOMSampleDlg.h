// CppCOMSampleDlg.h : header file
//

#pragma once

#include "GFAPIEvents.h"
#include "logstream.h"
#include "afxwin.h"

// CCppCOMSampleDlg dialog
class CCppCOMSampleDlg : public CDialog, public GF_Api_COM::CGFAPIEvents
{
// Construction
public:
	CCppCOMSampleDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CPPCOMSAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	GF_Api_COM::IGFComClientPtr& GFAPI()
	{
		if (!_GFAPI)
		{
			HRESULT hr = CoInitialize(NULL);
			if (!SUCCEEDED(hr))
			{
				log << "Error: " << hr << std::endl;
				return _GFAPI;
			}

			hr = _GFAPI.CreateInstance(__uuidof(GF_Api_COM::GFComClient));
			if (!SUCCEEDED(hr))
			{
				log << "Error: " << hr << std::endl;
				return _GFAPI;
			}

			AttachClient(_GFAPI);
			_GFClientRunner = _GFAPI->Threading->CreateRunnerFor(_GFAPI);
			_GFClientRunner->Start();
		}
		return _GFAPI;
	}

	virtual void __stdcall OnLoginComplete();
	virtual void __stdcall OnLoginFailed(GF_Api_COM::FailReason failReason);
	virtual void __stdcall OnDisconnected(GF_Api_COM::DisconnectionReason reason, BSTR message);
	virtual void __stdcall OnCommandUpdated(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::ICommandPtr command);
	virtual void __stdcall OnOrderConfirmed(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::IOrderIDPtr originalOrderId);
	virtual void __stdcall OnOrderFilled(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::IFillPtr fill);
	virtual void __stdcall OnOrderStateChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::OrderState previousState);
	virtual void __stdcall OnPriceChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IContractPtr contract, GF_Api_COM::IPricePtr price);
	virtual void __stdcall OnAvgPositionChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::IPositionPtr contractPosition);
	virtual void __stdcall OnAccountSummaryChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency);
	virtual void __stdcall OnBalanceChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency);
	virtual void __stdcall OnDomChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IContractPtr contract);
	virtual void __stdcall OnBarsReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::IBarListPtr bars);
	virtual void __stdcall OnTicksReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::ITickListPtr ticks);
	virtual void __stdcall OnErrorOccurred(GF_Api_COM::IGFComClientPtr client, mscorlib::_ExceptionPtr exception);
	virtual void __stdcall OnSymbolLookupReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISymbolLookupRequestIDPtr requestID, GF_Api_COM::IContractListPtr contracts);
	virtual void __stdcall OnAllocationBlocksChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAllocationBlockTemplateListPtr allocationBlockTemplates);
	virtual void __stdcall OnMarginCalculationCompleted(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IMarginCalculationResultPtr result);
	virtual void __stdcall OnNewMessageLogged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::LogCategory category, BSTR/*string*/ message);

	void showBalance(GF_Api_COM::IAccountPtr account, GF_Api_COM::IBalancePtr balance);
	void showDOM(GF_Api_COM::IContractPtr contract, GF_Api_COM::IUintListPtr sizes, GF_Api_COM::IFloatListPtr levels);
	void showAllocationBlocks(GF_Api_COM::IAllocationBlockTemplateListPtr& allocationBlocks);
	void showDataFeedEntitlements(GF_Api_COM::IDataFeedEntitlementListPtr& dataFeeds);

// Implementation
protected:
	HICON m_hIcon;
	logstream log;
	GF_Api_COM::IGFComClientPtr _GFAPI;
	GF_Api_COM::IGFComClientRunnerPtr _GFClientRunner;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedConnect();
	CListBox m_LogListBox;
	afx_msg void OnBnClickedSendorder();
	BOOL m_ShowPriceUpdates;
	afx_msg void OnBnClickedShowprices();
	CButton m_ShowPositions;
	BOOL m_bShowPositions;
	afx_msg void OnBnClickedShowpositions();
	BOOL m_ShowBalances;
	afx_msg void OnBnClickedShowbalances();
	afx_msg void OnBnClickedSubscribeprice();
	afx_msg void OnBnClickedShowdom();
	BOOL m_ShowDOMUpdates;
	afx_msg void OnBnClickedSubscribedom();
	afx_msg void OnBnClickedHistory();
	afx_msg void OnBnClickedContractdetails();
	afx_msg void OnBnClickedShoworders();
	afx_msg void OnBnClickedCancelorder();
	afx_msg void OnBnClickedModifyorder();
	afx_msg void OnBnClickedShowpos();
	afx_msg void OnBnClickedDisconnect();
	afx_msg void OnBnClickedBtnlookup();
	afx_msg void OnBnClickedBtnAccountInfo();
};

#define CheckGFAPI() if(!GFAPI()) return ;
#define CheckConnectedGFAPI() if(!GFAPI() || !GFAPI()->Connection->Aggregate->IsConnected) return ;