#pragma once
#include "afxwin.h"


// CSelectContract dialog

class CSelectContract : public CDialog
{
	DECLARE_DYNAMIC(CSelectContract)

public:
	CSelectContract(GF_Api_COM::IGFComClientPtr& _GFAPI, GF_Api_COM::ContractKind contractKind = (GF_Api_COM::ContractKind)(-1), CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectContract();

// Dialog Data
	enum { IDD = IDD_SELECTCONTRACT };

protected:
	GF_Api_COM::IGFComClientPtr& m_GFAPI;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual  BOOL OnInitDialog( );

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_Symbols;
	CString m_SelectedSymbol;
	GF_Api_COM::ContractKind m_ContractKind;
};
