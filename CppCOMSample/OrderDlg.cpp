// OrderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CppCOMSample.h"
#include "OrderDlg.h"


// COrderDlg dialog

IMPLEMENT_DYNAMIC(COrderDlg, CDialog)

COrderDlg::COrderDlg(GF_Api_COM::IGFComClientPtr& _GFAPI, GF_Api_COM::IOrderPtr _Original, CWnd* pParent /*=NULL*/)
	: CDialog(COrderDlg::IDD, pParent)
	, m_Qty(1)
	, m_SelectedContract(_T(""))
	, m_SelectedType(0)
	, m_Price(0)
	, m_GTC(FALSE)
	, m_GFAPI(_GFAPI)
	, m_Original(_Original)
	, m_AllocationBlock(false)
{
	if(m_Original != NULL)
	{
		m_Qty = m_Original->Quantity;
		m_SelectedContract = (LPCSTR)m_Original->Contract->Symbol;
		m_SelectedType = m_Original->Type;
		m_Price = m_Original->Price;
		m_GTC = m_Original->Flags == GF_Api_COM::OrderFlags_GTC;
	}
}

COrderDlg::~COrderDlg()
{
}

void COrderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_QTY, m_Qty);
	DDX_CBString(pDX, IDC_CONTRACTS, m_SelectedContract);
	DDX_CBIndex(pDX, IDC_TYPE, m_SelectedType);
	DDX_Text(pDX, IDC_PRICE, m_Price);
	DDX_Check(pDX, IDC_GTC, m_GTC);
	DDX_Control(pDX, IDC_CONTRACTS, m_Symbols);
	DDX_Control(pDX, IDC_TYPE, m_Types);
	DDX_Check(pDX, IDC_CHECK_AB, m_AllocationBlock);
}


BEGIN_MESSAGE_MAP(COrderDlg, CDialog)
	ON_BN_CLICKED(IDOK, &COrderDlg::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_CONTRACTS, &COrderDlg::OnCbnSelchangeContracts)
END_MESSAGE_MAP()

BOOL COrderDlg::OnInitDialog( )
{	
	__super::OnInitDialog();
	if(m_Original != NULL)
	{
		m_Symbols.AddString(m_Original->Contract->Symbol);
		m_Symbols.EnableWindow(FALSE);
	}
	else
	{
		CWaitCursor cursor;
		for(int i=0; i< m_GFAPI->Contracts->Get()->Count && i<100; ++i)
			m_Symbols.AddString(m_GFAPI->Contracts->Get()->GetAt(i)->Symbol);
	}
	m_Symbols.SetCurSel(0);
	m_Types.AddString("Market");
	m_Types.AddString("Limit");
	m_Types.AddString("Stop");
	m_Types.SetCurSel(0);
	CButton rbBuy, rbSell;
	rbBuy.Attach(::GetDlgItem(*this, IDC_BUY));
	rbSell.Attach(::GetDlgItem(*this, IDC_SELL));
	if(m_Original != NULL)
	{
		if(m_Original->Side == GF_Api_COM::OrderSide_Buy)
			rbBuy.SetCheck(TRUE);
		else
			rbSell.SetCheck(TRUE);
		rbBuy.EnableWindow(FALSE);
		rbSell.EnableWindow(FALSE);
		m_Types.SetCurSel(m_Original->Type);
	}
	else
		rbBuy.SetCheck(TRUE);
	rbBuy.Detach();
	rbSell.Detach();
	return TRUE;
}

// COrderDlg message handlers

void COrderDlg::OnBnClickedOk()
{
	CButton rbBuy;
	rbBuy.Attach(::GetDlgItem(*this, IDC_BUY));
	m_Buy = rbBuy.GetCheck();
	rbBuy.Detach();
	OnOK();
}

void COrderDlg::OnCbnSelchangeContracts()
{
	UpdateData(TRUE);
	GF_Api_COM::IContractPtr contract = m_GFAPI->Contracts->Get_2((LPCSTR)m_SelectedContract);
	if(contract != NULL)
	{
		GF_Api_COM::IPricePtr price = contract->CurrentPrice;
		if(price != NULL)
		{
			m_Price = price->LastPrice;
			UpdateData(FALSE);
		}
	}
}

