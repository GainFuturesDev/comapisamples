#pragma once


// CTicketNumberDlg dialog

class CTicketNumberDlg : public CDialog
{
	DECLARE_DYNAMIC(CTicketNumberDlg)

public:
	CTicketNumberDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTicketNumberDlg();

// Dialog Data
	enum { IDD = IDD_ORDERID };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_TicketNumber;
};
