// CppCOMSampleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CppCOMSample.h"
#include "CppCOMSampleDlg.h"
#include "LoginDlg.h"
#include "OrderDlg.h"
#include "SelectContract.h"
#include "TicketNumberDlg.h"
#include "SymbolLookupDlg.h"
#include <iostream>
#include <atlsafe.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCppCOMSampleDlg dialog


CCppCOMSampleDlg::CCppCOMSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCppCOMSampleDlg::IDD, pParent)
	, m_ShowPriceUpdates(FALSE)
	, m_bShowPositions(FALSE)
	, m_ShowBalances(FALSE)
	, m_ShowDOMUpdates(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCppCOMSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG, m_LogListBox);
	DDX_Check(pDX, IDC_SHOWPRICES, m_ShowPriceUpdates);
	DDX_Control(pDX, IDC_SHOWPOSITIONS, m_ShowPositions);
	DDX_Check(pDX, IDC_SHOWPOSITIONS, m_bShowPositions);
	DDX_Check(pDX, IDC_SHOWBALANCES, m_ShowBalances);
	DDX_Check(pDX, IDC_SHOWDOM, m_ShowDOMUpdates);
}

BEGIN_MESSAGE_MAP(CCppCOMSampleDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CONNECT, &CCppCOMSampleDlg::OnBnClickedConnect)
	ON_BN_CLICKED(IDC_SENDORDER, &CCppCOMSampleDlg::OnBnClickedSendorder)
	ON_BN_CLICKED(IDC_SHOWPRICES, &CCppCOMSampleDlg::OnBnClickedShowprices)
	ON_BN_CLICKED(IDC_SHOWPOSITIONS, &CCppCOMSampleDlg::OnBnClickedShowpositions)
	ON_BN_CLICKED(IDC_SHOWBALANCES, &CCppCOMSampleDlg::OnBnClickedShowbalances)
	ON_BN_CLICKED(IDC_SUBSCRIBEPRICE, &CCppCOMSampleDlg::OnBnClickedSubscribeprice)
	ON_BN_CLICKED(IDC_SHOWDOM, &CCppCOMSampleDlg::OnBnClickedShowdom)
	ON_BN_CLICKED(IDC_SUBSCRIBEDOM, &CCppCOMSampleDlg::OnBnClickedSubscribedom)
	ON_BN_CLICKED(IDC_HISTORY, &CCppCOMSampleDlg::OnBnClickedHistory)
	ON_BN_CLICKED(IDC_CONTRACTDETAILS, &CCppCOMSampleDlg::OnBnClickedContractdetails)
	ON_BN_CLICKED(IDC_SHOWORDERS, &CCppCOMSampleDlg::OnBnClickedShoworders)
	ON_BN_CLICKED(IDC_CANCELORDER, &CCppCOMSampleDlg::OnBnClickedCancelorder)
	ON_BN_CLICKED(IDC_MODIFYORDER, &CCppCOMSampleDlg::OnBnClickedModifyorder)
	ON_BN_CLICKED(IDC_SHOWPOS, &CCppCOMSampleDlg::OnBnClickedShowpos)
	ON_BN_CLICKED(IDC_DISCONNECT, &CCppCOMSampleDlg::OnBnClickedDisconnect)
	ON_BN_CLICKED(IDC_BTNLOOKUP, &CCppCOMSampleDlg::OnBnClickedBtnlookup)
	ON_BN_CLICKED(IDC_BTNACCOUNTINFO, &CCppCOMSampleDlg::OnBnClickedBtnAccountInfo)
END_MESSAGE_MAP()


// CCppCOMSampleDlg message handlers

BOOL CCppCOMSampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	log.rdbuf()->init(&m_LogListBox);
	log << "Welcome to GAIN Futures C++ COM sample. " << std::endl << "Please press Connect button" << std::endl;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCppCOMSampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCppCOMSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCppCOMSampleDlg::OnClose()
{
	if(_GFAPI != NULL)
	{
		DetachClient();
		_GFAPI->Connection->Aggregate->Disconnect();
		Sleep(100);
	}
	_GFAPI = NULL;
	CDialog::OnClose();
}

void CCppCOMSampleDlg::OnBnClickedConnect()
{
	CheckGFAPI();
	if(!GFAPI()->Connection->Aggregate->IsConnected)
	{
		CLoginDlg dlg;
		if(dlg.DoModal()== IDOK)
		{
			GF_Api_COM::IConnectionContextBuilderPtr connectionBuilder;
			connectionBuilder.CreateInstance(__uuidof(GF_Api_COM::ConnectionContextBuilder));

			auto context = connectionBuilder
				->WithUserName((LPCSTR)dlg.Username())
				->WithPassword((LPCSTR)dlg.Password())
				->WithUUID("9e61a8bc-0a31-4542-ad85-33ebab0e4e86")
				->WithHost((LPCSTR)dlg.Server())
				->WithPort(9210)
				->Build();

			GFAPI()->Connection->Aggregate->Connect(context);
			log << "Start connecting..." << std::endl;
		}
	}
}

void __stdcall CCppCOMSampleDlg::OnLoginComplete()
{
	log << "Login Completed" << std::endl;

	GF_Api_COM::ISymbolLookupRequestBuilderPtr builder;
	builder.CreateInstance(__uuidof(GF_Api_COM::SymbolLookupRequestBuilder));
	builder = builder->WithBaseSymbol("ES", GF_Api_COM::TextSearchMode_Exact);
	GFAPI()->Contracts->Lookup->ByCriteria(builder->Build());

	showAllocationBlocks(GFAPI()->Allocation->Get());
	showDataFeedEntitlements(GFAPI()->Exchanges->GetEntitlements());
}

void __stdcall CCppCOMSampleDlg::OnLoginFailed(GF_Api_COM::FailReason failReason)
{
	log << "Login  Failed: " << failReason << std::endl;
}

void __stdcall CCppCOMSampleDlg::OnDisconnected(GF_Api_COM::DisconnectionReason reason, BSTR message)
{
	CString string(message);
	log << "Disconnected due to " << string <<std::endl;
}

void CCppCOMSampleDlg::OnBnClickedSendorder()
{
	CheckConnectedGFAPI();
	COrderDlg dlg(GFAPI());
	if(dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IOrderDraftBuilderPtr builder;
		builder.CreateInstance(__uuidof(GF_Api_COM::OrderDraftBuilder));
		GF_Api_COM::IAccountListPtr accounts = GFAPI()->Accounts->Get();
		GF_Api_COM::IAccountPtr account = accounts->GetAt(0);
		GF_Api_COM::IAccountIDPtr accountID = account->id;

		if (dlg.m_AllocationBlock != 0)
			builder = builder->WithAllocationBlock(GFAPI()->Allocation->Get()->GetAt(0));
		else
			builder = builder->WithAccountID(accountID);

		builder = builder
			->WithSide(dlg.m_Buy ? GF_Api_COM::OrderSide_Buy : GF_Api_COM::OrderSide_Sell)
			->WithQuantity(dlg.m_Qty)
			->WithContractID(GFAPI()->Contracts->Get_2((LPCSTR)dlg.m_SelectedContract)->id)
			->WithOrderType((GF_Api_COM::OrderType)dlg.m_SelectedType)
			->WithFlags(dlg.m_GTC ? GF_Api_COM::OrderFlags_GTC : GF_Api_COM::OrderFlags_None);

		if (GFAPI()->Orders->Drafts->GetPriceCount((GF_Api_COM::OrderType)dlg.m_SelectedType) == 1)
			builder = builder->WithPrice(dlg.m_Price);

		GF_Api_COM::IOrderDraftPtr draft = builder->Build();

		if(GFAPI()->Orders->Drafts->Validate_2(draft)->Count > 0)
			log << "Order has invalid parts and will not be sent" << std::endl;
		else
		{
			GF_Api_COM::IOrderPtr order = GFAPI()->Orders->SendOrder(draft);
			if(!order)
				log << "Cannot send order" << std::endl;
			else
				log << "Order has been sent. " << order->id->Value << ": " << order->ToString << std::endl;
		}
	}
}

void __stdcall CCppCOMSampleDlg::OnOrderConfirmed(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::IOrderIDPtr originalOrderId)
{
	GF_Api_COM::IOrderPtr Order = order;
	GF_Api_COM::IOrderIDPtr OldOrderID = originalOrderId;
	log << "Order with temporary ID:" << OldOrderID->Value << " is confirmed: new ID: " << Order->id->Value << std::endl;
}

const char * stateToStr(GF_Api_COM::OrderState state)
{
	switch(state)
	{
	case GF_Api_COM::OrderState_Accepted:
		return "Accepted";
	case GF_Api_COM::OrderState_Cancelled:
		return "Cancelled";
	case GF_Api_COM::OrderState_Completed:
		return "Completed";
	case GF_Api_COM::OrderState_Held:
		return "Held";
	case GF_Api_COM::OrderState_None:
		return "None";
	case GF_Api_COM::OrderState_Rejected:
		return "Rejected";
	case GF_Api_COM::OrderState_Sent:
		return "Sent";
	case GF_Api_COM::OrderState_Unknown:
		return "Unknown";
	case GF_Api_COM::OrderState_Working:
		return "Working";
	}
	return "Invalid";
}

void __stdcall CCppCOMSampleDlg::OnOrderStateChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::OrderState previousState)
{
	log << "Order state changed. " << order->id->Value << ": " << order->ToString << ", " << stateToStr(order->CurrentState) << std::endl;
}

void __stdcall CCppCOMSampleDlg::OnOrderFilled(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::IFillPtr fill)
{
	if(fill->IsActive)
		log << "New fill. Order #" << order->id->Value << " " << fill->Quantity << "@" << fill->Price << " " << fill->Contract->Symbol << std::endl;
	else
		log << "Fill cancelled. Order #" << order->id->Value << " " << fill->Quantity << "@" << fill->Price << " " << fill->Contract->Symbol << std::endl;
}

void __stdcall CCppCOMSampleDlg::OnCommandUpdated(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IOrderPtr order, GF_Api_COM::ICommandPtr command)
{
	log << "Command updated. Order #" << order->id->Value << ", command: " << command->TypeState << std::endl;
}

void __stdcall CCppCOMSampleDlg::OnPriceChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IContractPtr contract, GF_Api_COM::IPricePtr price)
{
	if(m_ShowPriceUpdates)
	{
		log << "Price changed. " << contract->Symbol << ", last price: " << contract->PriceToString(price->LastPrice) << ", last vol: " << price->LastVol << std::endl;
	}
}

void __stdcall CCppCOMSampleDlg::OnAvgPositionChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::IPositionPtr contractPosition)
{
	if(m_bShowPositions)
	{
		log << "Avg. Position changed. " << account->Spec << ", " << contractPosition->DisplayContractSymbol << ", net pos: " << contractPosition->Net->Volume << "@" << contractPosition->Net->Price << ", P/L: " << contractPosition->Gain << std::endl;
	}
}

void CCppCOMSampleDlg::showBalance(GF_Api_COM::IAccountPtr account, GF_Api_COM::IBalancePtr balance)
{
		log << account->Spec << " (";
		if(balance->Currency != NULL)
			log << balance->Currency->name;
		else
			log << "$";
		log << "), Realized P/L: " << balance->RealizedPnL << ", Open P/L: " << balance->OpenPnL << ", initial margin: " << balance->InitialMargin;
		if(balance->PortfolioMargin != NULL)
			log << ", portfolio risk value: " << balance->PortfolioMargin->RiskValue;
		log << std::endl;
}

void __stdcall CCppCOMSampleDlg::OnAccountSummaryChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency)
{
	if(m_ShowBalances)
	{
		GF_Api_COM::IBalancePtr balance = account->TotalBalance;
		log << "Account Summary changed. ";
		showBalance(account, balance);
	}
}

void __stdcall CCppCOMSampleDlg::OnBalanceChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAccountPtr account, GF_Api_COM::ICurrencyPtr currency)
{
	if(m_ShowBalances && currency != NULL)
	{
		GF_Api_COM::IBalancePtr balance = account->Balances->GetValue(currency);
		log << "Balance changed. ";
		showBalance(account, balance);
	}
}

void CCppCOMSampleDlg::OnBnClickedShowprices()
{
	UpdateData(TRUE);
}

void CCppCOMSampleDlg::OnBnClickedShowpositions()
{
	UpdateData(TRUE);
}

void CCppCOMSampleDlg::OnBnClickedShowbalances()
{
	UpdateData(TRUE);
}

void CCppCOMSampleDlg::OnBnClickedSubscribeprice()
{
	CheckConnectedGFAPI();
	CSelectContract dlg(GFAPI());
	if(dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IContractPtr contract = GFAPI()->Contracts->Get_2((LPCSTR)dlg.m_SelectedSymbol);
		if (contract != NULL)
		{
			GF_Api_COM::ISubscriptionPtr priceSubscription = GFAPI()->Subscriptions->Price->Subscribe(contract->id);
			log << "Symbol " << contract->Symbol << " is subscribed for real-time priceswith sub id of " << priceSubscription->id->Value << std::endl;
		}
	}
}

void CCppCOMSampleDlg::OnBnClickedShowdom()
{
	UpdateData(TRUE);
}

void CCppCOMSampleDlg::OnBnClickedSubscribedom()
{
	CheckConnectedGFAPI();
	CSelectContract dlg(GFAPI());
	if (dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IContractPtr contract = GFAPI()->Contracts->Get_2((LPCSTR)dlg.m_SelectedSymbol);
		if (contract != NULL)
		{
			GF_Api_COM::ISubscriptionPtr domSubscription = GFAPI()->Subscriptions->Dom->Subscribe(contract->id);
			log << "Symbol " << contract->Symbol << " is subscribed for depth-of-market with sub id of " << domSubscription->id->Value << std::endl;
		}
	}
}

void CCppCOMSampleDlg::showDOM(GF_Api_COM::IContractPtr contract, GF_Api_COM::IUintListPtr sizes, GF_Api_COM::IFloatListPtr levels)
{
	int len = sizes->Count;
	for(int i=0; i<len; ++i)
		log << "        " << contract->PriceToString(levels->GetAt(i)) << ": " << sizes->GetAt(i) << std::endl;
}


void __stdcall CCppCOMSampleDlg::OnDomChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IContractPtr contract)
{
	if(m_ShowDOMUpdates)
	{
		//GF_Api_COM::IContractPtr Contract = contract;
		log << "DOM " << contract->Symbol << " changed." << std::endl;
		GF_Api_COM::IDomPtr dom = contract->Dom;
		GF_Api_COM::IUintListPtr askSizes = dom->AskSizes;
		GF_Api_COM::IFloatListPtr askLevels = dom->AskLevels;
		GF_Api_COM::IUintListPtr bidSizes = dom->BidSizes;
		GF_Api_COM::IFloatListPtr bidLevels = dom->BidLevels;
		log << "    " << "Ask" << std::endl;
		showDOM(contract, askSizes, askLevels);
		log << "    " << "Bid" << std::endl;
		showDOM(contract, bidSizes, bidLevels);
	}
}


void CCppCOMSampleDlg::OnBnClickedHistory()
{
	CheckConnectedGFAPI();
	CSelectContract dlg(GFAPI());
	if (dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IContractPtr contract = GFAPI()->Contracts->Get_2((LPCSTR)dlg.m_SelectedSymbol);
		GFAPI()->Subscriptions->Bars->Subscribe(
			contract->id,
			GFAPI()->Subscriptions->Bars->Duration->Create(0),
			GFAPI()->Subscriptions->Bars->Description->CreateMinutes(60)
			);
		log << "Historical data of " << dlg.m_SelectedSymbol << " are requested" << std::endl;
	}
}

void __stdcall CCppCOMSampleDlg::OnBarsReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::IBarListPtr bars)
{
	log << "Bars received: " << subscription->Contract->Symbol << ", " << bars->Count << " bars" << std::endl;
	log << "    Time, Open, High, Low, Close" << std::endl;
	for(int i=0; i< bars->Count; ++i)
	{
		GF_Api_COM::IBarPtr bar = bars->GetAt(i);
		COleDateTime dt(bar->CloseTimestamp);
		log << "    " << dt.Format() << ", " << bar->Open << ", " << bar->High << ", " << bar->Low << ", " << bar->Close << std::endl;
	}
}

void __stdcall CCppCOMSampleDlg::OnTicksReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISubscriptionPtr subscription, GF_Api_COM::ITickListPtr ticks)
{
	log << "Ticks received: " << std::endl;
	for(int i=0; i< ticks->Count; ++i)
	{
		double price = ticks->GetAt(i)->Price;
		log << "    " << price << std::endl;
	}
}

void CCppCOMSampleDlg::OnBnClickedContractdetails()
{
	CheckConnectedGFAPI();
	CSelectContract dlg(GFAPI());
	if(dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IContractPtr contract = GFAPI()->Contracts->Get_2((LPCSTR)dlg.m_SelectedSymbol);
		log << "Symbol: " << contract->Symbol << " (" << contract->Description << "), " << (contract->Type == GF_Api_COM::ContractType_Pit ? "Pit" : "Electronic");
		log << ", base symbol: " << contract->BaseSymbol;
		log << ", group: " << contract->ContractGroup->name << ", exchange: " << contract->Exchange->name << std::endl;
		log << "    Position symbol: " << contract->PositionSymbol << std::endl;
		log << "    Tick size: " << contract->tickSize << ", contract size: " << contract->ContractSize << ", " << contract->Currency->name << std::endl;
		log << "    Price format: " << contract->PriceToString(0) << ", multiplier: " << contract->priceMultiplier << std::endl;
		log << "    Expiration date: " << COleDateTime(contract->ExpirationDate).Format() << std::endl;
		log << "    Start time: " << COleDateTime(contract->StartTime).Format("%H:%M") << ", stop time: " << COleDateTime(contract->StopTime).Format("%H:%M") << std::endl;
	}
}

void CCppCOMSampleDlg::OnBnClickedShoworders()
{
	CheckConnectedGFAPI();
	GF_Api_COM::IOrderListPtr orders = GFAPI()->Orders->Get();
	log << "Orders: " << orders->Count << std::endl;
	for(int i=0; i<orders->Count; ++i)
	{
		GF_Api_COM::IOrderPtr order = orders->GetAt(i);
		log << "    #" << order->TicketNumber->Value << ": " << order << ", " << order->Fills->TotalQuantity << "/" << order->Quantity << ", " << stateToStr(order->CurrentState) << std::endl;
	}
}

void CCppCOMSampleDlg::OnBnClickedCancelorder()
{
	CheckConnectedGFAPI();
	CTicketNumberDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		GF_Api_COM::IOrderPtr order = GFAPI()->Orders->Get_2(GFAPI()->Orders->CreateID(dlg.m_TicketNumber));
		if(!order)
			MessageBox("Wrong ticket number");
		else
		{
			log << "Cancel request for #" << dlg.m_TicketNumber << ": " << order->ToString << std::endl;
			GFAPI()->Orders->CancelOrder(order->id, NULL);
		}
	}
}

void CCppCOMSampleDlg::OnBnClickedModifyorder()
{
	CheckConnectedGFAPI();
	CTicketNumberDlg dlgTicket;
	if(dlgTicket.DoModal() == IDOK)
	{
		GF_Api_COM::IOrderPtr order = GFAPI()->Orders->Get_2(GFAPI()->Orders->CreateID(dlgTicket.m_TicketNumber));
		if(!order)
			MessageBox("Wrong ticket number");
		else
		{
			COrderDlg dlg(GFAPI(), order);
			if(dlg.DoModal() == IDOK)
			{
				GF_Api_COM::IModifyOrderDraftBuilderPtr builder;
				builder.CreateInstance(__uuidof(GF_Api_COM::ModifyOrderDraftBuilder));

				GF_Api_COM::IModifyOrderDraftPtr draft =
					builder->FromOrder(order)
						->WithQuantity(dlg.m_Qty)
						->WithOrderType((GF_Api_COM::OrderType)dlg.m_SelectedType)
						->WithPrice(dlg.m_Price)
						->Build();


				if(GFAPI()->Orders->Drafts->Validate(draft)->Count > 0)
					log << "Order has invalid parts and will not be sent" << std::endl;
				else
				{
					GF_Api_COM::IOrderPtr order = GFAPI()->Orders->ModifyOrder(draft);
					if (!order)
						log << "Cannot send order" << std::endl;
					else
						log << "Order has been sent. " << order->id->Value << ": " << order->ToString << std::endl;
				}
			}
		}
	}
}

void CCppCOMSampleDlg::OnBnClickedShowpos()
{
	CheckConnectedGFAPI();

	GF_Api_COM::IAccountListPtr accountList = GFAPI()->Accounts->Get();
	for (int a = 0; a < accountList->Count; a++)
	{
		GF_Api_COM::IAccountPtr account = accountList->GetAt(a);
		log << "Account: " << account->Spec << std::endl;
		GF_Api_COM::IAvgPositionDictionaryPtr posList = account->AvgPositions;
		log << "Avg. Positions: " << posList->Count << std::endl;
		for (int i = 0; i < posList->Count; ++i)
		{
			GF_Api_COM::IPositionPtr pos = posList->ValueByIndex(i);
			log << "    " << pos->DisplayContractSymbol << ". prev: " << pos->Prev->Volume << ", #sold: " << pos->Short->Volume << ", #bought: " << pos->Long->Volume << ", P/L: " << pos->Gain << ", initial margin: " << pos->InitialMargin << std::endl;
		}
	}
	if (GFAPI()->Allocation->Get()->Count > 0)
	{
		for (int ab = 0; ab < GFAPI()->Allocation->Get()->Count; ab++)
		{
			GF_Api_COM::IAllocationBlockPtr block = GFAPI()->Allocation->Create(GFAPI()->Allocation->Get()->GetAt(ab));
			GF_Api_COM::IAvgPositionDictionaryPtr posList2 = block->AvgPositions;
			log << "Avg. Positions of Allocation Block " << block->name << ": " << posList2->Count << std::endl;
			for (int i = 0; i < posList2->Count; ++i)
			{
				GF_Api_COM::IPositionPtr pos = posList2->ValueByIndex(i);
				log << "    " << pos->DisplayContractSymbol << ". prev: " << pos->Prev->Volume << ", #sold: " << pos->Short->Volume << ", #bought: " << pos->Long->Volume << ", P/L: " << pos->Gain << ", initial margin: " << pos->InitialMargin << std::endl;
			}
		}
	}
}

void CCppCOMSampleDlg::OnErrorOccurred(GF_Api_COM::IGFComClientPtr client, mscorlib::_ExceptionPtr ex)
{
	_bstr_t stackTrace = ex->StackTrace;
	if(!stackTrace)
		stackTrace = "";
	else
		stackTrace = " at " + stackTrace;
	_bstr_t message = ex->Message;
	log << "OnError: " << message << stackTrace << std::endl;
}

void CCppCOMSampleDlg::OnNewMessageLogged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::LogCategory category, BSTR/*string*/ message)
{
	log << "OnNewMessageLogged (" << category << "): " << message << std::endl;
}

void CCppCOMSampleDlg::OnBnClickedDisconnect()
{
	if(_GFAPI != NULL && _GFAPI->Connection->Aggregate->IsConnected)
	{
		_GFAPI->Connection->Aggregate->Disconnect();
	}
}

void CCppCOMSampleDlg::OnSymbolLookupReceived(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::ISymbolLookupRequestIDPtr requestID, GF_Api_COM::IContractListPtr contracts)
{
	log << "Contracts received: " << contracts->Count << std::endl;
	GF_Api_COM::IMarginCalculationRequestBuilderPtr builder;
	builder.CreateInstance(__uuidof(GF_Api_COM::MarginCalculationRequestBuilder));
	GF_Api_COM::IAccountListPtr accs = GFAPI()->Accounts->Get();
	GF_Api_COM::IAccountPtr account = accs->GetAt(0);
	if(builder != NULL)
	{
		builder = builder->WithAccountID(account->id);
	}
	int hypoQty = 1;
	for(int i=0; i< contracts->Count; ++i)
	{
		GF_Api_COM::IContractPtr contract = contracts->GetAt(i);
		log << "    " << contract->Symbol << ": " << contract->BaseContract->Description;
		if(contract->IsFuture || contract->IsOption)
			log << ", initial margin: " << contract->GetInitialMargin(account);
		if(contract->IsOption)
			log << ", strike display factor: " << contract->BaseContract->StrikeDisplayFactor;
		log << std::endl;
		if(contract->Legs->Count > 0)
		{
			log << "        Legs: " << contract->Legs->Count << endl;
			for(int i=0; i<contract->Legs->Count; ++i)
			{
				GF_Api_COM::ILegPtr leg = contract->Legs->GetAt(i);
				log << "              " << leg->Contract->Symbol << endl;
			}
		}
		else
		{
			if(builder != NULL)
			{
				builder = builder->WithPosition(GFAPI()->Margin->CreateHypoPosition(contract->id, hypoQty, hypoQty));
				hypoQty = -hypoQty;
			}
		}
	}
	if(builder != NULL)
	{
		GF_Api_COM::IMarginCalculationRequestPtr request = builder->Build();
		if (request->Positions->Count > 0)
		{
			int requestID = GFAPI()->Margin->RequestMarginCalculation(request);
			log << "MarginCalculatorRequest sent with regular contracts above and +/-1 qty. RequestID: " << requestID << endl;
		}
	}
}

void __stdcall CCppCOMSampleDlg::OnMarginCalculationCompleted(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IMarginCalculationResultPtr result)
{
	log << "MarginCalculatorResponse. RequestID: " << result->RequestID << endl;
	log << "InitialMarginRequirements: " << result->InitialMarginRequirements << endl;
	log << "MaintenanceMarginRequirements: " << result->MaintenanceMarginRequirements << endl;
	log << "NetOptionsValue: " << result->NetOptionValue << endl;
	log << "RiskValue: " << result->RiskValue << endl;
}

void CCppCOMSampleDlg::OnBnClickedBtnlookup()
{
	CheckConnectedGFAPI();
	CSymbolLookupDlg dlg;
	if (dlg.DoModal() == IDOK)
	{
		GF_Api_COM::ISymbolLookupRequestBuilderPtr builder;
		builder.CreateInstance(__uuidof(GF_Api_COM::SymbolLookupRequestBuilder));

		GF_Api_COM::ISymbolLookupExpressionBuilderPtr expressionBuilder;
		expressionBuilder.CreateInstance(__uuidof(GF_Api_COM::SymbolLookupExpressionBuilder));

		GF_Api_COM::IContractKindListPtr contractKindList;
		contractKindList.CreateInstance(__uuidof(GF_Api_COM::ContractKindList));

		if (dlg.m_chkFutures)
			contractKindList->Add(GF_Api_COM::ContractKind_Future);
		if (dlg.m_chkOptions)
			contractKindList->Add(GF_Api_COM::ContractKind_Option);
		if (dlg.m_Spreads)
		{
			contractKindList->Add(GF_Api_COM::ContractKind_GenericCompound);
			contractKindList->Add(GF_Api_COM::ContractKind_FutureCompound);
			contractKindList->Add(GF_Api_COM::ContractKind_OptionsCompound);
		}

		builder = builder
			->WithSymbol((LPCSTR)dlg.m_strLookupText, (GF_Api_COM::TextSearchMode)dlg.m_indxMode)
			->WithResultCount(dlg.m_numContracts);

		if (contractKindList->Count > 0)
			builder = builder->WithExpression(expressionBuilder->WithContractKinds(contractKindList)->Build());

		GF_Api_COM::ISymbolLookupRequestPtr request = builder->Build();
		GFAPI()->Contracts->Lookup->ByCriteria(request);
	}
}

void CCppCOMSampleDlg::OnBnClickedBtnAccountInfo()
{
	CheckConnectedGFAPI();
	GF_Api_COM::IAccountListPtr accs = GFAPI()->Accounts->Get();
	for (int i = 0; i < accs->Count; i++)
	{
		GF_Api_COM::IAccountPtr account = accs->GetAt(i);
		log << "Account ID: " << account->id->Value << endl;
		log << "Account Spec: " << account->Spec << endl;
		log << "Account Type: " << account->Type << endl;
		log << "Account IsComplianceRiskLimit: " << (account->IsComplianceRiskLimit == 0 ? "false" : "true") << endl;
		log << "Account RiskLimitsType: " << account->_RiskLimitsType << endl;
	}
}

void CCppCOMSampleDlg::OnAllocationBlocksChanged(GF_Api_COM::IGFComClientPtr client, GF_Api_COM::IAllocationBlockTemplateListPtr allocationBlockTemplates)
{
	showAllocationBlocks(allocationBlockTemplates);
}

void CCppCOMSampleDlg::showAllocationBlocks(GF_Api_COM::IAllocationBlockTemplateListPtr& allocationBlocks)
{
	log << "Allocation Blocks: " << allocationBlocks->Count << endl;
	for(int i=0; i<allocationBlocks->Count; ++i)
	{
		GF_Api_COM::IAllocationBlockTemplatePtr abtemplate = allocationBlocks->GetAt(i);
		GF_Api_COM::IAllocationBlockPtr ab = GFAPI()->Allocation->Create(abtemplate);
		log << "    " << ab->name << " total lots: " << ab->TotalLots << " rule: " << ab->rule << endl;
		for(int j=0; j<ab->items->Count; ++j)
		{
			GF_Api_COM::IAllocationBlockItemPtr abi = ab->items->ValueByIndex(j);
			log << "        " << abi->Account->Spec << ": " << abi->Lots << endl;
		}
	}
}

void CCppCOMSampleDlg::showDataFeedEntitlements(GF_Api_COM::IDataFeedEntitlementListPtr& dataFeeds)
{
	log << "DataFeedEntitlements: " << dataFeeds->Count << endl;
	for (int i = 0; i < dataFeeds->Count; ++i)
	{
		GF_Api_COM::IDataFeedEntitlementPtr df = dataFeeds->GetAt(i);
		log << "    " <<  " Type: " << df->Type << "; Exchange: " << df->Exchange->name << endl;
	}
}
