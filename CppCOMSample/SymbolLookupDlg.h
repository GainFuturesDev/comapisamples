#pragma once
#include "afxwin.h"


// CSymbolLookupDlg dialog

class CSymbolLookupDlg : public CDialog
{
	DECLARE_DYNAMIC(CSymbolLookupDlg)

public:
	CSymbolLookupDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSymbolLookupDlg();

// Dialog Data
	enum { IDD = IDD_LOOKUP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_strLookupText;
	CComboBox m_cmbMode;
	int m_numContracts, m_indxMode;
	BOOL m_chkFutures;
	BOOL m_chkForex;
	BOOL m_Spreads;
	BOOL m_chkOptions;
};
